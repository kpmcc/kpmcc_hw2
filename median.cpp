#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

double
sort_median(vector<double> v)
{
    sort(v.begin(), v.end());
    return v[v.size() / 2];
}

// couldn't get these working

// double
// partial_sort_median(vector<double> v, double * pivot)
// {
//     std::partial_sort(v.begin(), pivot, v.end());
//     return v[v.size() / 2];
// }

// double
// nth_elem_median(vector<double> v, double * nth )
// {
//     std::nth_elem(v.begin(), nth, v.end());
//     return v[v.size() /2 ];
// }

int main()
{
    return 0;
}
