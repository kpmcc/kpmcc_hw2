#include <algorithm>
#include <vector>
#include <random>
#include <iostream>
#include <functional>
#include <cmath>

using namespace std;

vector<float> random_float_vector(float min, float max, unsigned int num_elems)
{
  vector<float> new_vec;
  default_random_engine generator;
  uniform_real_distribution<float> distribution(min, max);
  auto randfloat = std::bind (distribution, generator);

  for (int i = 0; i < num_elems; i++)
    new_vec.push_back(randfloat());

  return new_vec;
}

float square(float n)
{
  return n*n;
}

float my_square(float n, float x)
{
  return n+(x*x);
}

vector<float> square_vec(vector<float> v)
{
  vector<float> new_vec;
  new_vec.resize(v.size());

  std::transform(v.begin(), v.end(), new_vec.begin(), square);

  return new_vec;
}



float vec_euclidean_dist(vector<float> v)
{
  return sqrt(accumulate(v.begin(), v.end(), 0));
}


int main()
{
  float min = 0.0;
  float max = 20.0;
  unsigned int num_elements = 10;
  auto rand_vec = random_float_vector(min, max, num_elements);
  
  vector<float> new_vec = square_vec(rand_vec);
  // std::transform(foo.begin(), foo.end(), bar.begin(), square);


  for (int i = 0; i < rand_vec.size(); i++)
    std::cout << rand_vec[i] << "\tsquared is\t" << new_vec[i] << "\n";

  cout << "Three arg accumulate\n";
  cout << vec_euclidean_dist(new_vec) << "\n";
  cout << "Four arg accumulate\n";
  float init = 0;
  cout << sqrt(accumulate(rand_vec.begin(), rand_vec.end(), init, my_square));

  
  return 0;
}
