#include <vector>
#include <numeric>
#include <iostream>
#include <string>
#include <iomanip>
#include <algorithm>
#include <sstream>
using namespace std;

string
centered_Int(int i, int eltsize)
{
  string s = to_string(i);
  return string((eltsize - s.size())/2, ' ') + s;
}

class Row {
public:
  Row get_next() {
    vector<int> result;
    int previous = 0;
    for (auto elt : my_nums){
      result.push_back(previous + elt);
      previous = elt;
    }
    result.push_back(previous);
    Row new_row(result);
    return new_row;
  }

  int max_numDigits() {
    int max = 0;
    int curr;
    for (auto elt : my_nums){
      curr = to_string(elt).size();
      if (curr > max)
        max = curr;
    }
    return max;
  }

  void print_row(int eltsize){
    for (auto it = my_nums.begin(); it!=my_nums.end(); it++){
      // cout << to_string((*it)) << " ";
      cout << left << setw(eltsize) << centered_Int((*it), eltsize) << " ";
    }
    cout << endl;
  }

  Row(vector<int> nums){
    my_nums = nums;
  }

  Row(){
    vector<int> nums;
    my_nums = nums;
  }

  vector<int> my_nums;

};

class Triangle {
public:
  Triangle(int n_rows){
    vector<Row> new_rows;

    vector<int> first;
    first.push_back(1);

    Row previous_row(first);
    Row next_row;
    new_rows.push_back(previous_row);
    for(int i = 1; i < n_rows; i++){
      next_row = previous_row.get_next();
      new_rows.push_back(next_row);
      previous_row = next_row;
    }
    my_rows = new_rows;
    num_rows = n_rows;
  }

  void print_me(){
    int eltsize = my_rows[my_rows.size() -1].max_numDigits();
    for(auto it = my_rows.begin(); it!=my_rows.end(); it++){
      (*it).print_row(eltsize);
    }
  }

  int num_rows;
  vector<Row> my_rows;
};

int main()
{
  Triangle new_tri{8};
  new_tri.print_me();
}
